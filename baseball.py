import numpy as np
import pandas
from sklearn import svm
from sklearn import preprocessing


input_file = "data/pitches.csv"

def get_data(n):

    # comma delimited is the default
    data = pandas.read_csv(input_file, header = 0, nrows=int(n))

    # cleanse data
    clean_data = data[~data.isin([np.nan, np.inf, -np.inf]).any(1)]
    data = clean_data

    # encode the target classes
    target_names = ['CH', 'CU', 'EP', 'FC', 'FF', 'FO', 'PO', 'FS', 'FT', 'IN', 'KC', 'KN', 'SC', 'SI', 'SL', 'UN']
    pitch_type_encoder = preprocessing.LabelEncoder()
    pitch_type_encoder.fit(target_names)
    target_classes = pitch_type_encoder.transform(list(data['pitch_type']))

    # encoding the pitch end result types
    end_result_types = ['S', 'B', 'X']
    end_result_type_encoder = preprocessing.LabelEncoder()
    end_result_type_encoder.fit(end_result_types)
    end_result_codes = end_result_type_encoder.transform(list(data['type']))
    data['end_result_code'] = pandas.Series(end_result_codes, index=data.index)

    # Removing non-numeric or believed to be irrelevant data columns
    data = data.drop(columns=['ab_id', 'code', 'on_1b', 'on_2b', 'on_3b', 'type', 'pitch_type'])
    feature_names = list(data.columns.values)
    return data, target_classes, feature_names, target_names
