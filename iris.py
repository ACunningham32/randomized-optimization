import numpy as np
from sklearn.datasets import load_iris

# Load the Iris dataset
data = load_iris()

print(data.target)
print(type(data.target))