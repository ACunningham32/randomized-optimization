import numpy as np
import pandas
import mlrose
import income
import time
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

input_file = "data/pitches.csv"
algorithms = ['gradient_descent', 'random_hill_climb', 'simulated_annealing', 'genetic_alg']
# algorithms = ['random_hill_climb', 'simulated_annealing']
# algorithms = ['genetic_alg']
sample_sizes = [1000, 3000, 5000, 7000, 9000, 11000, 15000]
# step_size = [0.1, 0.2, 0.3, 0.5, 0.7, 0.8, 0.9]
step_size = [0.1]

def get_baseball_data(n):

    # comma delimited is the default
    data = pandas.read_csv(input_file, header = 0, nrows=int(n))

    # cleanse data
    clean_data = data[~data.isin([np.nan, np.inf, -np.inf]).any(1)]
    data = clean_data

    target = list(data['pitch_type'])
    # print(target) 

    # encoding the pitch end result types
    end_result_types = ['S', 'B', 'X']
    end_result_type_encoder = preprocessing.LabelEncoder()
    end_result_type_encoder.fit(end_result_types)
    end_result_codes = end_result_type_encoder.transform(list(data['type']))
    data['end_result_code'] = pandas.Series(end_result_codes, index=data.index)

    # encode the target classes
    target_names = ['CH', 'CU', 'EP', 'FC', 'FF', 'FO', 'PO', 'FS', 'FT', 'IN', 'KC', 'KN', 'SC', 'SI', 'SL', 'UN']
    pitch_type_encoder = preprocessing.LabelEncoder()
    pitch_type_encoder.fit(target_names)
    target_names_enc = pitch_type_encoder.transform(target_names)
    target_enc = pitch_type_encoder.transform(target)

    # data = data.drop(columns=['ab_id', 'code', 'on_1b', 'on_2b', 'on_3b', 'type', 'pitch_type'])
    X = data.drop(columns=['ab_id', 'code', 'on_1b', 'on_2b', 'on_3b', 'type', 'pitch_type'])
    # y = data['pitch_type']

    # Split data into training and test sets
    X_train, X_test, y_train, y_test = train_test_split(X, target_enc, \
                                                    test_size = 0.2, random_state = 3)

    scaler = preprocessing.MinMaxScaler()
    scaler.fit(X_train)

    X_train_scaled = scaler.transform(X_train)
    X_test_scaled = scaler.transform(X_test)

    # One hot encode target values
    one_hot = preprocessing.OneHotEncoder(categories='auto')
    y_train_hot = one_hot.fit_transform(y_train.reshape(-1, 1)).todense()
    y_test_hot = one_hot.transform(y_test.reshape(-1, 1)).todense()

    return X_train_scaled, X_test_scaled, y_train_hot, y_test_hot, target_names_enc

def weight_exists(new_weights, all_weights):
    for i in range(len(all_weights)):
        if all_weights[i].shape == new_weights.shape and np.allclose(all_weights[i], new_weights, atol=1e-05):
            return i
    
    return -1

file_name = str(time.time())
results_file = open("results/{}.txt".format(file_name),"w")
weights = []
total_time_start = time.time()
results_file.write("Parameters:\n")
results_file.write("\nhidden_nodes: {}".format(4))
results_file.write("\nmax_attetmpts: {}".format(1000))
results_file.write("\nlearning_rate: {}".format(0.5))
results_file.write("\nactivation: {}".format('relu'))
results_file.write("\nschedule: {}".format('geom decay'))
results_file.write("\nmutation prob: {}".format('0.1'))
results_file.write("\nclip max: {}".format('3'))

for sample_size in sample_sizes:
    for step in step_size:

        X_train, X_test, y_train, y_test, target_enc = get_baseball_data(sample_size)

        # Initialize neural network object and fit object
        np.random.seed(3)

        for a in algorithms:
            nn_model1 = mlrose.NeuralNetwork(hidden_nodes = [4], activation = 'relu', \
                                            algorithm = a, max_iters = 1000, \
                                            bias = True, is_classifier = True, learning_rate = step, \
                                            early_stopping = True, clip_max = 3, max_attempts = 1000,
                                            schedule = mlrose.GeomDecay(), mutation_prob = 0.1)

            train_time_start = time.time()
            nn_model1.fit(X_train, y_train)
            train_time = time.time() - train_time_start

            # print(type(nn_model1.fitted_weights))
            match_idx = weight_exists(nn_model1.fitted_weights, weights)
            if match_idx == -1:
                weights.append(nn_model1.fitted_weights)

            else:
                output = "\nMATCHED WEIGHTS: {}, a: {}, samples: {}\n".format(match_idx, a, sample_size)
                print(output)
                results_file.write(output)

            # Predict labels for train set and assess accuracy
            train_pedict_time_start = time.time()
            y_train_pred = nn_model1.predict(X_train)
            train_pedict_time = time.time() - train_pedict_time_start
            y_train_accuracy = accuracy_score(y_train, y_train_pred)

            # print(y_train_accuracy)

            # Predict labels for test set and assess accuracy
            test_predict_time_start = time.time()
            y_test_pred = nn_model1.predict(X_test)
            test_predict_time = time.time() - test_predict_time_start
            y_test_accuracy = accuracy_score(y_test, y_test_pred)

            # print(y_test_accuracy)

            final_outputs = []
            final_outputs.append("\n")
            final_outputs.append("{}: {} samples\n".format(a, sample_size))
            final_outputs.append("y_train_accuracy: {}\n".format(round(y_train_accuracy, 4) * 100))
            final_outputs.append("y_test_accuracy: {}\n".format(round(y_test_accuracy, 4) * 100))
            final_outputs.append("fit_time: {}\n".format(round(train_time, 4)))
            final_outputs.append("train_pedict_time: {}\n".format(round(train_pedict_time, 4)))
            final_outputs.append("test_predict_time: {}\n".format(round(test_predict_time, 4)))

            for outputs in final_outputs:
                print(outputs)
                results_file.write(outputs)

total_time = time.time() - total_time_start
end_output1 = "length of weights: {}\n".format(len(weights))
end_output2 = "Total processing time: {}\n".format(round(total_time, 4))
print(end_output1)
print(end_output2)
results_file.write(end_output1)
results_file.write(end_output2)
results_file.close()

