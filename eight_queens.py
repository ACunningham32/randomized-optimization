import mlrose
import numpy as np

# Define alternative N-Queens fitness function for maximization problem
def queens_max(state):

   # Initialize counter
	fitness_cnt = 0

	# For all pairs of queens
	for i in range(len(state) - 1):
			for j in range(i + 1, len(state)):

				# Check for horizontal, diagonal-up and diagonal-down attacks
				if (state[j] != state[i]) \
					and (state[j] != state[i] + (j - i)) \
					and (state[j] != state[i] - (j - i)):

					# If no attacks, then increment counter
					fitness_cnt += 1

	return fitness_cnt

# Initialize custom fitness function object
fitness_cust = mlrose.CustomFitness(queens_max)
fitness = mlrose.Queens()

problem = mlrose.DiscreteOpt(length = 8, fitness_fn = fitness, maximize = False, max_val = 8)

# Define decay schedule
schedule = mlrose.ExpDecay()

# Define initial state
init_state = np.array([0, 1, 2, 3, 4, 5, 6, 7])

# Set random seed
np.random.seed(1)

# Solve problem using simulated annealing
best_state, best_fitness = mlrose.simulated_annealing(problem, schedule = schedule,
													  max_attempts = 10, max_iters = 1000,
													  init_state = init_state)

print(best_state)
# [6 4 7 3 6 2 5 1]

print(best_fitness)
# 2.0