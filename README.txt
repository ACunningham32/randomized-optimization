README.txt

Neural Network:
Find my repo at: https://bitbucket.org/ACunningham32/randomized-optimization/src/master/

1. git clone

2. create an empty folder in the root of the project called "results"

3. pip install -r requirements.txt

4. python neural_network.py
    This is to run the neural network portion of the project.
    At the top of neural_network.py, you can set the algorithms you want to run, and over how many samples.


Optimization Problems:
Find my repo at: https://bitbucket.org/ACunningham32/randomized-optimization-fitness/src/master/

1. git clone

2. create an empty folder in the root of the project called "results"

3. pip install -r requirements.txt

4. python fitness.py
    At the top of fitness.py you can set knapsack to True for Knapsack probelm of False for the other problems.
    You can modify which fitness problem to run and what samples to run them for.