import mlrose
import numpy as np

fitness = mlrose.OneMax()
problem = mlrose.DiscreteOpt(length = 8, fitness_fn = fitness, maximize = True)
schedule = mlrose.ExpDecay()
init_state = np.array([0, 0, 0, 0, 0, 0, 0, 0])
np.random.seed(1)
best_state, best_fitness = mlrose.simulated_annealing(problem, schedule = schedule,
                                                      max_attempts = 1000, max_iters = 1000,
                                                      init_state = init_state)

print(best_state)
print(best_fitness)